// This file allows you to add content to the mission without conflict issues after each update of the original classnames.sqf
// If you want more modifications to be supported by this file, let's discuss it on the forums.



// *** SUPPORT STUFF ***

// Setting a value here will overwrite the original value found from the mission. Do that if you're doing a total conversion.
// Each of these should be unique, the same classnames for different purposes may cause various unpredictable issues with player actions. Or not. Just don't try.
FOB_typename = nil;                                     // Default "Land_Cargo_HQ_V1_F";
FOB_box_typename = nil;                                 // Default "B_Slingload_01_Cargo_F";
FOB_truck_typename = nil;                               // Default "B_Truck_01_box_F";
Arsenal_typename = nil;                                 // Default "B_supplyCrate_F";
Respawn_truck_typename = nil;                           // Default "B_Truck_01_medical_F";
huron_typename = "RHS_CH_47F_light";                    // Default "B_Heli_Transport_03_unarmed_F";
ammobox_b_typename = nil;                               // Default "Box_NATO_AmmoVeh_F";
ammobox_o_typename = nil;                               // Default "Box_East_AmmoVeh_F";
opfor_ammobox_transport = "RHS_Ural_Open_VDV_01";       // Default "O_Truck_03_transport_F";    // Make sure this thing can transport ammo boxes (see box_transport_config down below) otherwise things will break
commander_classname = "rhsusf_army_ocp_officer";        // Default "B_officer_F"
crewman_classname = "rhsusf_army_ocp_crewman";          // Default "B_crew_F";
pilot_classname = nil;                                  // Default "B_Helipilot_F";





// *** FRIENDLIES ***

// Each array below represents one page of the build menu
// Format : [ "classname", manpower, ammo, fuel ]
// Example : [ "B_APC_Tracked_01_AA_F", 0, 40, 15 ],

// If overwrite is set to true, then the extension list will entirely replace the original list defined in classnames.sqf. Otherwise it will be appended to it.
// Useful for total conversions to RHS and such, without having to alter the original file.
infantry_units_overwrite = true;
infantry_units_extension = [
    ["rhsusf_army_ocp_rifleman",2,0,0],
    ["rhsusf_army_ocp_grenadier",3,0,0],
    ["rhsusf_army_ocp_autorifleman",3,0,0],
    ["rhsusf_army_ocp_medic",3,0,0],
    ["rhsusf_army_ocp_engineer",3,0,0],
    ["rhsusf_army_ocp_riflemanat",4,0,0],
    ["rhsusf_army_ocp_machinegunner",5,0,0],
    ["rhsusf_army_ocp_marksman",8,0,0],
    ["rhsusf_army_ocp_aa",5,10,0],
    ["rhsusf_army_ocp_javelin",5,10,0],
    ["rhsusf_army_ocp_sniper",10,0,0],
    ["rhsusf_army_ocp_rifleman_101st",2,0,0],
    ["rhsusf_army_ocp_combatcrewman",1,0,0],
    ["rhsusf_army_ocp_helipilot",1,0,0]
];

light_vehicles_overwrite = true;
light_vehicles_extension = [
    ["B_Quadbike_01_F",0,0,1],
    ["rhsusf_m1025_d",0,0,2],
    ["rhsusf_m1025_d_m2",0,15,2],
    ["rhsusf_m1025_d_Mk19",0,25,2],
    ["rhsusf_M998_D_2DR",0,0,3],
    ["rhsusf_rg33_d",0,0,5],
    ["rhsusf_rg33_m2_d",0,10,5],
    ["B_Truck_01_transport_F",0,0,5],
    ["B_Truck_01_covered_F",0,0,5],
    ["rhsusf_M1083A1P2_B_M2_f_fmtv_usarmy",0,10,5],
    ["B_Boat_Transport_01_F",0,0,2],
    ["CUP_B_RHIB_USMC",0,25,10],
    ["B_Boat_Armed_01_minigun_F",0,50,10],
    ["CUP_B_Jackal2_GMG_GB_W",0,50,10],
    ["CUP_B_M1126_ICV_M2_Desert",0,65,10],
    ["CUP_B_HMMWV_TOW_USMC",0,75,15],
    ["CUP_B_HMMWV_Avenger_USMC",0,100,15],
    ["CUP_B_M1128_MGS_Desert",0,150,15]
];

heavy_vehicles_overwrite = true;
heavy_vehicles_extension = [
    ["rhsusf_m113d_usarmy_unarmed",0,0,15],
    ["rhsusf_m113d_usarmy",0,25,15],
    ["rhsusf_m113d_usarmy_MK19",0,40,15],
    ["CUP_B_M163_USA",0,125,15],
    ["CUP_B_AAV_USMC",0,100,20],
    ["CUP_B_LAV25M240_USMC",0,125,20],
    ["RHS_M2A2",0,175,25],
    ["CUP_O_BTR90_RU",0,185,25],
    ["RHS_M2A3",0,200,25],
    ["RHS_M2A3_BUSKIII",0,225,25],
    ["RHS_M6",0,175,20],
    ["rhsusf_m1a1aimwd_usarmy",0,250,30],
    ["rhsusf_m1a2sep1d_usarmy",0,300,30],
    ["rhsusf_m109d_usarmy",0,250,20],
    ["CUP_B_M270_HE_USMC",0,400,20]
];

air_vehicles_overwrite = true;
air_vehicles_extension = [
    ["B_UAV_01_F",0,0,5],
    ["B_UAV_02_F",0,150,20],
    ["B_UAV_02_CAS_F",0,250,20],
    ["CUP_B_UH1D_GER_KSK",0,10,25],
    ["RHS_UH60M_d",0,50,25],
    ["RHS_UH60M_MEV2_d",0,50,25],
    ["RHS_CH_47F",0,50,20],
    ["CUP_B_MV22_USMC",0,0,40],
    ["MELB_MH6M",0,0,10],
    ["MELB_AH6M_L",0,75,10],
    ["MELB_AH6M_M",0,100,10],
    ["MELB_AH6M_H",0,125,10],
    ["CUP_B_AW159_Cannon_GB",0,100,20],
    ["RHS_UH1Y",0,150,25],
    ["RHS_AH1Z_GS",0,200,40],
    ["RHS_AH64D_GS",0,300,40],
    ["RHS_AH1Z",0,300,50],
    ["RHS_AH64D",0,400,50],
    ["CUP_B_AV8B_AGM_USMC",0,500,60],
    ["RHS_A10",0,600,50],
    ["rhsusf_f22",0,700,100],
    ["CUP_B_F35B_CAS_BAF",0,800,100]
];

static_vehicles_overwrite = true;
static_vehicles_extension = [
    ["RHS_M2StaticMG_MiniTripod_D",0,10,0],
    ["RHS_M2StaticMG_D",0,10,0],
    ["RHS_MK19_TriPod_D",0,20,0],
    ["rhsgref_cdf_b_ZU23",0,25,0],
    ["RHS_Stinger_AA_pod_D",0,50,0],
    ["B_Mortar_01_F",0,100,0],
    ["RHS_TOW_TriPod_D",0,150,0],
    ["RHS_M119_D",0,200,0]
];

buildings_overwrite = false;
buildings_extension = [
];

// If you're going to overwrite this, make sure you have at least
// Arsenal_typename, Respawn_truck_typename, FOB_box_typename and
// FOB_truck_typename in there
support_vehicles_overwrite = false;		
support_vehicles_extension = [
    ["rhsusf_m113d_usarmy_supply",10,30,10],
    ["rhsusf_m113d_usarmy_medical",10,0,10]
];

// All the UAVs must be declared here, otherwise there shall be UAV controlling issues. Namely: you won't be able to control them.
uavs = [
    "CUP_USMC_MQ9"
];

// Pre-made squads for the commander build menu. These shouldn't exceed 10 members.
// Light infantry squad
blufor_squad_inf_light = [
    "rhsusf_army_ucp_squadleader",
    "rhsusf_army_ucp_teamleader",
    "rhsusf_army_ocp_grenadier",
    "rhsusf_army_ocp_autorifleman",
    "rhsusf_army_ocp_grenadier",
    "rhsusf_army_ocp_medic",
    "rhsusf_army_ocp_riflemanat",
    "rhsusf_army_ocp_rifleman",
    "rhsusf_army_ocp_rifleman"
];

// Heavy infantry squad
blufor_squad_inf = [
    "rhsusf_army_ucp_squadleader",
    "rhsusf_army_ucp_teamleader",
    "rhsusf_army_ocp_autorifleman",
    "rhsusf_army_ocp_machinegunner",
    "rhsusf_army_ocp_medic",
    "rhsusf_army_ocp_grenadier",
    "rhsusf_army_ocp_riflemanat",
    "rhsusf_army_ocp_riflemanat",
    "rhsusf_army_ocp_marksman",
    "rhsusf_army_ocp_marksman"
];

// AT specialists squad
blufor_squad_at = [
    "rhsusf_army_ucp_squadleader",
    "rhsusf_army_ocp_javelin",
    "rhsusf_army_ocp_javelin",
    "rhsusf_army_ocp_javelin",
    "rhsusf_army_ocp_medic",
    "rhsusf_army_ocp_rifleman"
];

// AA specialists squad
blufor_squad_aa = [
    "rhsusf_army_ucp_squadleader",
    "rhsusf_army_ocp_aa",
    "rhsusf_army_ocp_aa",
    "rhsusf_army_ocp_aa",
    "rhsusf_army_ocp_medic",
    "rhsusf_army_ocp_rifleman"
];

// Force recon squad
blufor_squad_recon = [
    "rhsusf_army_ucp_teamleader",
    "rhsusf_army_ocp_rifleman",
    "rhsusf_army_ocp_engineer",
    "rhsusf_army_ocp_medic",
    "rhsusf_army_ocp_riflemanat",
    "rhsusf_army_ocp_riflemanat",
    "rhsusf_army_ocp_marksman",
    "rhsusf_army_ocp_rifleman",
    "rhsusf_army_ocp_rifleman"
];

// Paratroopers squad
blufor_squad_para = [
    "rhsusf_army_ocp_rifleman_101st",
    "rhsusf_army_ocp_rifleman_101st",
    "rhsusf_army_ocp_rifleman_101st",
    "rhsusf_army_ocp_rifleman_101st",
    "rhsusf_army_ocp_rifleman_101st",
    "rhsusf_army_ocp_rifleman_101st",
    "rhsusf_army_ocp_rifleman_101st",
    "rhsusf_army_ocp_rifleman_101st",
    "rhsusf_army_ocp_rifleman_101st",
    "rhsusf_army_ocp_rifleman_101st"
];



// *** BADDIES ***

// All OPFOR infantry. Defining a value here will replace the default value from the original mission.
opfor_sentry = "rhs_vdv_flora_rifleman";
opfor_rifleman = "rhs_vdv_flora_rifleman";
opfor_grenadier = "rhs_vdv_flora_grenadier";
opfor_squad_leader = "rhs_vdv_flora_sergeant";
opfor_team_leader = "rhs_vdv_flora_junior_sergeant";
opfor_marksman = "rhs_vdv_flora_marksman";
opfor_machinegunner = "rhs_vdv_flora_machinegunner";
opfor_heavygunner = "rhs_msv_arifleman";
opfor_medic = "rhs_vdv_flora_medic";
opfor_rpg = "rhs_vdv_flora_LAT";
opfor_at = "rhs_vdv_flora_grenadier_rpg";
opfor_aa = "rhs_vdv_flora_aa";
opfor_officer = "rhs_vdv_flora_officer";
opfor_sharpshooter = "rhs_vdv_flora_marksman";
opfor_sniper = "rhs_msv_marksman";
opfor_engineer = "rhs_vdv_flora_engineer";
opfor_paratrooper = "rhs_vdv_flora_rifleman";


// OPFOR Vehicles to be used in secondary objectives
opfor_mrap = "rhs_tigr_vdv";
opfor_mrap_armed = "rhs_tgr_sts_vdv";
opfor_transport_helo = "RHS_Mi8AMT_vvs";
opfor_transport_truck = "rhs_gaz66_msv";
opfor_fuel_truck = "RHS_Ural_Fuel_MSV_01";
opfor_ammo_truck = "rhs_gaz66_ammo_msv";
opfor_fuel_container = "B_Slingload_01_Fuel_F";
opfor_ammo_container = "B_Slingload_01_Ammo_F";
opfor_flag = "rhs_Flag_Russia_F";

// Militia infantry. Soldier classnames the game will pick from randomly
militia_squad_overwrite = true;
militia_squad_extension = [
    "rhsgref_ins_squadleader",
    "rhsgref_ins_specialist_aa",
    "rhsgref_ins_macinegunner",
    "rhsgref_ins_medic",
    "rhsgref_ins_engineer",
    "rhsgref_ins_rifleman_RPG26",
    "rhsgref_ins_militiaman_mosin",
    "rhsgref_ins_militiaman_mosin",
    "rhsgref_ins_militiaman_mosin",
    "rhsgref_ins_militiaman_mosin",
    "rhsgref_ins_militiaman_mosin",
    "rhsgref_ins_militiaman_mosin",
    "rhsgref_ins_militiaman_mosin",
    "rhsgref_ins_grenadier",
    "rhsgref_ins_grenadier_rpg"
];

// Militia vehicles to choose from
militia_vehicles_overwrite = true;
militia_vehicles_extension = [
    "rhsgref_ins_g_zsu234",
    "rhsgref_ins_btr60",
    "rhsgref_ins_btr70",
    "rhsgref_ins_uaz",
    "rhsgref_ins_uaz_open",
    "rhsgref_ins_uaz_ags",
    "rhsgref_ins_uaz_spg9",
    "rhsgref_ins_bmd1",
    "rhsgref_ins_bmd2",
    "rhsgref_BRDM2_HQ_ins",
    "rhsgref_BRDM2_ins",
    "rhsgref_ins_uaz_dshkm",
    "CUP_O_T72_CHDKZ",
    "rhsgref_ins_BM21",
    "rhsgref_ins_ural",
    "rhsgref_ins_ural_open",
    "rhsgref_ins_ural_Zu23",
    "rhsgref_ins_gaz66_zu23",
    "CUP_O_BMP2_ZU_TKA",
    "CUP_O_Datsun_PK_Random"
];

// All the vehicles that can spawn as sector defenders and patrols
opfor_vehicles_overwrite = true;
opfor_vehicles_extension = [
    "rhs_btr70_vdv",
    "rhs_btr80_vdv",
    "rhs_bmd2m",
    "rhs_bmd4_vdv",
    "rhs_bmp2d_vdv",
    "rhs_t80u",
    "rhs_t72bd_tv",
    "RHS_BM21_VDV_01",
    "rhs_zsu234_aa",
    "rhs_t90a_tv",
    "rhs_t80um",
    "rhs_tigr_sts_vdv",
    "rhs_btr80a_vv",
    "CUP_O_BTR90_RU",
    "rhs_9k79",
    "rhs_KORD_high_VDV",
    "rhs_Metis_9k115_2_msv",
    "rhs_Igla_AA_pod_vdv",
    "rhs_Kornet_9M133_2_vdv"
];

// Same with lighter choices to be used  when the alert level is low
opfor_vehicles_low_intensity_overwrite = true;
opfor_vehicles_low_intensity_extension = [
    "rhs_btr60_vdv",
    "rhs_tigr_vdv",
    "rhs_tigr_3camo_vdv",
    "rhs_uaz_open_vdv",
    "rhs_uaz_vdv",
    "RHS_Ural_VDV_01",
    "rhs_9k79",
    "rhs_KORD_high_VDV",
    "rhs_tigr_m_3camo_vdv",
    "rhs_tigr_sts_3camo_vdv",
    "rhs_tigr_sts_vdv",
    "CUP_O_UAZ_METIS_RU",
    "CUP_O_BMP2_ZU_TKA",
    "rhsgref_BRDM2_vdv",
    "rhsgref_BRDM2_ATGM_vdv",
    "rhsgref_BRDM2_vdv",
    "rhsgref_BRDM2_ATGM_vdv",
    "rhsgref_BRDM2_HQ_vdv"
];

// All the vehicles that can spawn as battlegroup members
opfor_battlegroup_vehicles_overwrite = true;
opfor_battlegroup_vehicles_extension = [
    "rhs_btr70_vdv",
    "rhs_btr80_vdv",
    "rhs_bmd2m",
    "rhs_bmd4_vdv",
    "rhs_bmp2d_vdv",
    "rhs_t80u",
    "rhs_t72bd_tv",
    "RHS_BM21_VDV_01",
    "rhs_zsu234_aa",
    "RHS_Mi24V_vdv",
    "RHS_Mi8MTV3_vdv",
    "RHS_Mi8MTV3_UPK23_vdv",
    "RHS_Ka52_vvsc",
    "rhs_bmp3m_msv",
    "rhs_bmp3_late_msv",
    "rhs_bmp3m_msv",
    "rhs_bmp3mera_msv"
];

// Same with lighter choices to be used  when the alert level is low
opfor_battlegroup_vehicles_low_intensity_overwrite = true;
opfor_battlegroup_vehicles_low_intensity_extension = [
    "rhs_tigr_vdv",
    "rhs_tigr_3camo_vdv",
    "rhs_uaz_open_vdv",
    "rhs_uaz_vdv",
    "rhs_btr60_vdv",
    "RHS_Ural_VDV_01",
    "RHS_Mi8mt_vdv",
    "rhs_uaz_spg9_chdkz",
    "rhs_uaz_dshkm_chdkz",
    "rhs_uaz_ags_chdkz"
];

// All the vehicles that can spawn as battlegroup members (see above) and also hold 8 soldiers as passengers.
// If something in here can't hold all 8 soldiers then buggy behaviours may occur
opfor_troup_transports_overwrite = true;
opfor_troup_transports_extension = [
    "rhs_btr70_vdv",
    "rhs_btr80_vdv",
    "rhs_bmd2m",
    "rhs_bmd4_vdv",
    "rhs_bmp2d_vdv",
    "RHS_Mi8MTV3_vdv",
    "RHS_Mi8MTV3_UPK23_vdv",
    "rhs_tigr_vdv",
    "rhs_tigr_3camo_vdv",
    "rhs_uaz_open_vdv",
    "rhs_uaz_vdv",
    "rhs_btr60_vdv",
    "RHS_Ural_VDV_01",
    "RHS_Mi8mt_vdv"
];

// Battlegroup members that will need to spawn in flight. Should be only helos but, who knows
opfor_choppers_overwrite = true;
opfor_choppers_extension = [
    "RHS_Mi24P_vdv",
    "RHS_Mi8MTV3_vdv",
    "RHS_Mi8MTV3_UPK23_vdv",
    "RHS_Mi8mt_vdv",
    "RHS_Ka52_vvsc",
    "CUP_O_Ka50_RU",
    "CUP_O_Mi24_D_TK"
];

// Opfor military aircrafts
opfor_air_overwrite = true;
opfor_air_extension = [
    "RHS_Su25SM_vvsc",
    "RHS_Su25SM_KH29_vvsc",
    "RHS_T50_vvs_051",
    "CUP_O_SU34_LGB_RU"
];


// Other stuff

// civilians
civilians_overwrite = false;
civilians_extension = [

];

// civilian vehicles
civilian_vehicles_overwrite = false;
civilian_vehicles_extension = [

];

// Everything the AI troups should be able to resupply from
ai_resupply_sources_extension = [
    "rhsusf_m113d_usarmy_supply"
];

// Everything that can resupply other vehicles
vehicle_repair_sources_extension = [

];
vehicle_rearm_sources_extension = [

];
vehicle_refuel_sources_extension = [

];

// Elite vehicles that should be unlocked through military base capture.
elite_vehicles_overwrite = true;
elite_vehicles_extension = [
    "rhsusf_m109d_usarmy",
    "RHS_AH64D",
    "RHS_A10",
    "RHS_AH1Z",
    "CUP_B_F35B_CAS_BAF",
    "rhsusf_f22",
    "CUP_B_M270_HE_USMC",
    "RHS_M2A3_BUSKIII",
    "rhsusf_m1a2sep1d_usarmy",
    "CUP_O_BTR90_RU",
    "rhsusf_m1a1aimwd_usarmy"
];

// Blacklisted arsenal items such as deployable weapons  that should be bought instead
// Useless if you're using a predefined arsenal in arsenal.sqf
blacklisted_from_arsenal_extension = [
	"RHS_Podnos_Bipod_Bag",
	"RHS_Podnos_Gun_Bag",
	"RHS_Metis_Gun_Bag",
	"RHS_Metis_Tripod_Bag",
	"RHS_AGS30_Tripod_Bag",
	"RHS_AGS30_Gun_Bag",
	"RHS_DShkM_Gun_Bag",
	"RHS_DShkM_TripodHigh_Bag",
	"RHS_DShkM_TripodLow_Bag",
	"RHS_Kord_Tripod_Bag",
	"RHS_Kord_Gun_Bag",
	"RHS_M2_Gun_Bag",
	"RHS_M2_Tripod_Bag",
	"rhs_M252_Gun_Bag",
	"rhs_M252_Bipod_Bag",
	"RHS_M2_MiniTripod_Bag",
	"RHS_Mk19_Gun_Bag",
	"RHS_Mk19_Tripod_Bag",
	"RHS_NSV_Tripod_Bag",
	"RHS_NSV_Gun_Bag",
	"RHS_SPG9_Gun_Bag",
	"RHS_SPG9_Tripod_Bag",
	"rhs_Tow_Gun_Bag",
	"rhs_TOW_Tripod_Bag"
];

// Configuration for ammo boxes transport
// First entry: classname
// Second entry: how far behind the vehicle the boxes should be unloaded
// Following entries: attachTo position for each box, the number of boxes that can be loaded is derived from the number of entries
box_transport_config_extension = [
	[ "greuh_eh101_gr", -6.5, [0,	4.2,	-1.45], [0,	2.5,	-1.45], [0,	0.8, -1.45], [0,	-0.9, -1.45] ],
    [ "RHS_Ural_Open_VDV_01", -5.5, [0,	-0.5,	0.35], [0,	-2.1,	0.35], [0,	-1.25,	1.85] ],
    [ "RHS_Ural_VDV_01", -5.5, [0,	-0.5,	0.35], [0,	-2.1,	0.35] ],
    [ "RHS_CH_47F", -8.5, [0,	2.0,	-1.8], [0,	0.0,	-1.8], [0,	-2.0,	-1.8] ],
	[ "rhsusf_M1083A1P2_B_M2_f_fmtv_usarmy", -6.5, [0,	-0.4,	0.4], [0,	-2.1,	0.4] ],
    [ "CUP_B_MV22_USMC", -8.5, [0,	2.0,	-1.8], [0,	0.0,	-1.8], [0,	-2.0,	-1.8]  ]
];
